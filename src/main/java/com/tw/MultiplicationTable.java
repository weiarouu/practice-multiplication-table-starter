package com.tw;

import java.util.ArrayList;

public class MultiplicationTable {
    public String buildMultiplicationTable(int start, int end) {
        Boolean validInput = isValid(start, end);
        if (!validInput) {
            return null;
        } else {
            return generateTable(start, end);
        }
    }

    public Boolean isValid(int start, int end) {
        Boolean isStartInRange = isInRange(start);
        Boolean isEndInRange = isInRange(end);
        Boolean isStartNumberNotBiggerThanEndNumber = isStartNotBiggerThanEnd(start, end);
        return isStartInRange && isEndInRange && isStartNumberNotBiggerThanEndNumber;
    }

    public Boolean isInRange(int number) {
        return number >= 1 && number <= 1000;
    }

    public Boolean isStartNotBiggerThanEnd(int start, int end) {
        return start <= end;
    }

    public String generateTable(int start, int end) {
        String multiplicationTable = null;
        ArrayList<String> Lines = new ArrayList<String>();

        for (int currentRow = start; currentRow <= end; currentRow++) {
            String currentLine = generateLine(start, currentRow);
            Lines.add(currentLine);
        }
        multiplicationTable = String.join(String.format("%n"), Lines);

        return multiplicationTable;
    }

    public String generateLine(int start, int row) {
        String resultLine = null;
        ArrayList<String> expressions = new ArrayList<String>();
        for (int currentMultiplicand = start; currentMultiplicand <= row; currentMultiplicand++) {
            String currentExpression = generateSingleExpression(currentMultiplicand, row);
            expressions.add(currentExpression);
        }
        resultLine = String.join("  ", expressions);
        return resultLine;
    }

    public String generateSingleExpression(int multiplicand, int multiplier) {
        return String.format("%d*%d=%d", multiplicand, multiplier, multiplicand*multiplier);
    }
}
